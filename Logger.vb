﻿Imports System.IO
Imports System.Security.AccessControl
Imports System.Diagnostics
Imports System.Reflection
Imports System.Configuration

Public Class Logger

    Public Shared Folder As String = Convert.ToString(ConfigurationManager.AppSettings("JHLog"))
    Public Shared UseDetailedException As Boolean = True
    Public Shared CurrentProcessingFileName As String
    Private Const HTML_LOG_FILE As String = "JHDataImpoterLog.html"
    ''' <summary>
    ''' Writes the given exception into log file
    ''' </summary>
    ''' <param name="ex"></param>
    ''' <remarks></remarks>
    Public Shared Sub LogException(ByVal ex As Exception)
        WriteEntryHTML("--")  'writes a blank line        
        WriteEntryHTML(getColoredText("ERROR OCCURED: ", "red") & ex.Message, 2)
        If UseDetailedException Then
            WriteEntryHTML("--ERROR DETAILS: " & ex.StackTrace, 2)
        End If
    End Sub
    Public Shared Sub LogEvent(ByVal eventType As String, Optional ByVal eventDetails As String = "")
        WriteEntryHTML("--")  'writes a blank line   
        Dim eventlog As String
        eventlog = getColoredText("EVENT: ", "blue") & eventType
        If Not eventDetails = String.Empty Then
            eventlog = eventlog + "<br>--<b>EVENT DETAILS:</b> " & eventDetails
        End If
        WriteEntryHTML(eventlog, 2)
    End Sub

    ''' <summary>
    ''' write the error message into log with style applied to it.
    ''' </summary>
    ''' <param name="Msg"></param>
    ''' <param name="FrameIndex"></param>
    ''' <remarks></remarks>
    Private Shared Sub WriteEntryHTML(ByVal Msg As String, Optional ByVal FrameIndex As Integer = 1)
        Dim stackTrace As New StackTrace()
        Dim stackFrame As StackFrame = stackTrace.GetFrame(FrameIndex)
        Dim methodBase As MethodBase = stackFrame.GetMethod()
        If methodBase IsNot Nothing Then
            If Left(Msg, 2) = "--" Then
                If Len(Msg) = 2 Then
                    WriteLog(HTML_LOG_FILE, "")
                Else
                    Msg = Mid(Msg, 3)
                    WriteLog(HTML_LOG_FILE, Msg)
                End If
            Else
                WriteLog(HTML_LOG_FILE, getColoredText(GetLogFormat() & ": ", "green") & getColoredText(getAssemblyNameWithoutExtension(methodBase.DeclaringType.Module.Name), "black", True) & "." & getColoredText(methodBase.DeclaringType.Name, "brown", True) & "." & getColoredText(methodBase.Name & ": ", "brown") & Msg)
            End If
        End If
    End Sub
    ''' <summary>
    ''' This function is used to return assembly name.
    ''' </summary>
    ''' <param name="FullAssemblyName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getAssemblyNameWithoutExtension(ByVal FullAssemblyName As String) As String
        If Right(FullAssemblyName, 4).ToLower = ".dll" Or Right(FullAssemblyName, 4).ToLower = ".exe" Then
            If Len(FullAssemblyName) > 4 Then
                Return Left(FullAssemblyName, Len(FullAssemblyName) - 4)
            Else
                Return ""
            End If
        Else
            Return ""
        End If
        'Return MethodBase.DeclaringType.Module.Name
    End Function
    ''' <summary>
    ''' This function is used to write entry in log with styles.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub WriteProgramStartLine()
        Dim startline As String = "JH DataImport V1.0.1 Log"
        startline = startline + "<br> ----Service Started on " + DateTime.Now.ToString()
        WriteEntryHTML("--<hr>", 2)
        WriteEntryHTML(getColoredText(startline, "blue"), 2)        
    End Sub
    Public Shared Sub WriteProgramEndLine()
        Dim startline = "<br> ----Service Stopped on " + DateTime.Now.ToString()        
        WriteEntryHTML(getColoredText(startline, "blue"), 2)
        WriteEntryHTML("--<hr>", 2)
    End Sub
    ''' <summary>
    ''' This function is used to write entry in log with styles.
    ''' </summary>
    ''' <param name="Msg"></param>
    ''' <param name="mycolor"></param>
    ''' <param name="FontBold"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getColoredText(ByVal Msg As String, ByVal mycolor As String, Optional ByVal FontBold As Boolean = False) As String
        Dim boldtagStart As String = ""
        Dim boldtagEnd As String = ""
        If FontBold Then
            boldtagStart = "<b>"
            boldtagEnd = "</b>"
        End If
        Return boldtagStart & "<FONT COLOR=" & Chr(34) & mycolor & Chr(34) & ">" + Msg + "</FONT>" & boldtagEnd
    End Function
    ''' <summary>
    ''' This function is used to write entry in log.
    ''' </summary>
    ''' <param name="FileName"></param>
    ''' <param name="sErrMsg"></param>
    ''' <remarks></remarks>
    Private Shared Sub WriteLog(ByVal FileName As String, ByVal sErrMsg As String)
        If Folder = "" Then
            Folder = "C:\IN\"
        End If
        If Folder.Length > 0 Then
            Dim dinfo As New DirectoryInfo(Folder)
            If Not dinfo.Exists Then
                dinfo.Create()
                dinfo.Refresh()
                System.Threading.Thread.Sleep(1000)
            End If
            ErrorLog(dinfo.FullName, FileName, sErrMsg)
        End If
    End Sub

    ''' <summary>
    ''' Log format like Date time 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetLogFormat() As String
        'sLogFormat used to create log files format :
        ' dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
        Dim sLogFormat As String = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() & "  "
        'this variable used to create log filename format "
        'for example filename : ErrorLogYYYYMMDD
        'string sYear = DateTime.Now.Year.ToString();
        'string sMonth = DateTime.Now.Month.ToString();
        'string sDay = DateTime.Now.Day.ToString();
        'string sErrorTime = sYear + sMonth + sDay;
        Return sLogFormat
    End Function

    ''' <summary>
    ''' This is the method where the actual text writing into file is done.
    ''' </summary>
    ''' <param name="sFolderPath"></param>
    ''' <param name="FileName"></param>
    ''' <param name="sErrMsg"></param>
    ''' <remarks></remarks>
    Private Shared Sub ErrorLog(ByVal sFolderPath As String, ByVal FileName As String, ByVal sErrMsg As String)
        Dim sw As StreamWriter = Nothing
        Dim sPathName As String = Path.Combine(sFolderPath, FileName)
        Try
            MoveContent(sFolderPath, FileName)
            If Not File.Exists(sPathName) Then
                ' Create a file to write to.
                sw = File.CreateText(sPathName)
                Dim style = "<STYLE TYPE=""text/css"">"
                style = style + "BODY{font-family:Calibri;"
                style = style + "font-size: 14px;font-weight: Bold;}"
                style = style + "</STYLE>"
                sw.WriteLine(style)
            Else
                ' This text is always added, making the file longer over time
                ' if it is not deleted.
                'using (StreamWriter sw = File.AppendText(sPathName))
                sw = File.AppendText(sPathName)
            End If
            sw.WriteLine(sErrMsg & "<br>")
        Catch ex As Exception
        Finally
            If sw IsNot Nothing Then
                sw.Close()
                sw.Dispose()
            End If
        End Try

    End Sub

    ''' <summary>
    ''' If log file size becomes large then an archive file is created and log file starts from zero size.
    ''' </summary>
    ''' <param name="sFolderPath"></param>
    ''' <param name="FileName"></param>
    ''' <remarks></remarks>
    Private Shared Sub MoveContent(ByVal sFolderPath As String, ByVal FileName As String)
        Dim fs As FileStream = Nothing
        Dim sPathName As String = Path.Combine(sFolderPath, FileName)
        Try
            Dim prvslogfilename As String
            prvslogfilename = Path.Combine(Folder, "JHDataImpoterLog.html")
            fs = System.IO.File.Open(sPathName, FileMode.Open, FileAccess.Read)
            '1MB =>1024 X KB
            '5MB =>1024 X 5
            '5242880 5MB
            ' 4MB 409600
            If fs.Length >= 5242880 Then
                'limit to 512MB
                fs.Close()
                fs.Dispose()
                If File.Exists(prvslogfilename) Then
                    'remove PreviousLog  
                    System.IO.File.Delete(prvslogfilename)
                End If
                'System.IO.File.Delete(sPathName);    
                System.IO.File.Move(sPathName, prvslogfilename)
            End If
        Catch ex As Exception
        Finally
            If fs IsNot Nothing Then
                fs.Close()
            End If
        End Try
    End Sub
End Class

