using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using System.Reflection;
using System.Configuration;

public class CustomLogger
{
    public static string Folder = Convert.ToString(ConfigurationManager.AppSettings.Get("LogFilePath"));
    public static bool UseDetailedException = true;
    public static string CurrentProcessingFileName;
    private const string HTML_LOG_FILE = "EventLog.html";
    /// <summary>
    /// Writes the given exception into log file
    /// </summary>
    /// <param name="ex"></param>
    /// <remarks></remarks>
    public static void LogException(Exception ex)
    {
        WriteEntryHTML("--");
        //writes a blank line        
        WriteEntryHTML(getColoredText("ERROR OCCURED: ", "red") + ex.Message, 2);
        if (UseDetailedException)
        {
            WriteEntryHTML("--ERROR DETAILS: " + ex.StackTrace, 2);
        }
    }
    public static void LogEvent(string eventType, string eventDetails = "")
    {
        WriteEntryHTML("--");
        //writes a blank line   
        string eventlog = null;
        eventlog = getColoredText("EVENT: ", "blue") + eventType;
        if (!(eventDetails == string.Empty))
        {
            eventlog = eventlog + "<br>--<b>EVENT DETAILS:</b> " + eventDetails;
        }
        WriteEntryHTML(eventlog, 2);
    }

    /// <summary>
    /// write the error message into log with style applied to it.
    /// </summary>
    /// <param name="Msg"></param>
    /// <param name="FrameIndex"></param>
    /// <remarks></remarks>
    private static void WriteEntryHTML(string Msg, int FrameIndex = 1)
    {
        StackTrace stackTrace = new StackTrace();
        StackFrame stackFrame = stackTrace.GetFrame(FrameIndex);
        MethodBase methodBase = stackFrame.GetMethod();
        if (methodBase != null)
        {
            if ((Msg.Substring(0, 2) == "--"))
            {
                if ((Msg.Length == 2))
                {
                    WriteLog(HTML_LOG_FILE, "");
                }
                else
                {
                    Msg = Msg.Substring(2);
                    WriteLog(HTML_LOG_FILE, Msg);
                }

            }
            else
            {
                WriteLog(HTML_LOG_FILE, (getColoredText((GetLogFormat() + ": "), "green")
                                + (getColoredText(getAssemblyNameWithoutExtension(methodBase.DeclaringType.Module.Name), "black", true) + ("."
                                + (getColoredText(methodBase.DeclaringType.Name, "brown", true) + ("."
                                + (getColoredText((methodBase.Name + ": "), "brown") + Msg)))))));
            }
        }
    }
    /// <summary>
    /// This function is used to return assembly name.
    /// </summary>
    /// <param name="FullAssemblyName"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    private static string getAssemblyNameWithoutExtension(string FullAssemblyName)
    {
        if (FullAssemblyName.Substring(FullAssemblyName.Length - 4).ToLower() == ".dll" || FullAssemblyName.Substring(FullAssemblyName.Length - 4).ToLower() == ".exe")
        {
            if ((FullAssemblyName.Length > 4))
            {
                return FullAssemblyName.Substring(0, (FullAssemblyName.Length - 4));
            }
            else
            {
                return "";
            }

        }
        else
        {
            return "";
        }

        //Return MethodBase.DeclaringType.Module.Name
    }
    /// <summary>
    /// This function is used to write entry in log with styles.
    /// </summary>
    /// <remarks></remarks>
    public static void WriteProgramStartLine()
    {
        string startline = "JH DataImport V1.0.1 Log";
        startline = startline + "<br> ----Service Started on " + DateTime.Now.ToString();
        WriteEntryHTML("--<hr>", 2);
        WriteEntryHTML(getColoredText(startline, "blue"), 2);
    }
    public static void WriteProgramEndLine()
    {
        dynamic startline = "<br> ----Service Stopped on " + DateTime.Now.ToString();

        WriteEntryHTML("--<hr>", 2);
    }
    /// <summary>
    /// This function is used to write entry in log with styles.
    /// </summary>
    /// <param name="Msg"></param>
    /// <param name="mycolor"></param>
    /// <param name="FontBold"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    private static string getColoredText(string Msg, string mycolor, bool FontBold = false)
    {
        string boldtagStart = "";
        string boldtagEnd = "";
        if (FontBold)
        {
            boldtagStart = "<b>";
            boldtagEnd = "</b>";
        }

        return (boldtagStart + ("<FONT COLOR=" + ('\"'
                    + (mycolor + ('\"'
                    + ((">"
                    + (Msg + "</FONT>"))
                    + boldtagEnd))))));
    }
    /// <summary>
    /// This function is used to write entry in log.
    /// </summary>
    /// <param name="FileName"></param>
    /// <param name="sErrMsg"></param>
    /// <remarks></remarks>
    private static void WriteLog(string FileName, string sErrMsg)
    {
        if (string.IsNullOrEmpty(Folder))
        {
            Folder = "C:\\IN\\";
        }
        if (Folder.Length > 0)
        {
            DirectoryInfo dinfo = new DirectoryInfo(Folder);
            if (!dinfo.Exists)
            {
                dinfo.Create();
                dinfo.Refresh();
                System.Threading.Thread.Sleep(1000);
            }
            ErrorLog(dinfo.FullName, FileName, sErrMsg);
        }
    }

    /// <summary>
    /// Log format like Date time 
    /// </summary>
    /// <returns></returns>
    /// <remarks></remarks>
    private static string GetLogFormat()
    {
        //sLogFormat used to create log files format :
        // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
        string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + "  ";
        //this variable used to create log filename format "
        //for example filename : ErrorLogYYYYMMDD
        //string sYear = DateTime.Now.Year.ToString();
        //string sMonth = DateTime.Now.Month.ToString();
        //string sDay = DateTime.Now.Day.ToString();
        //string sErrorTime = sYear + sMonth + sDay;
        return sLogFormat;
    }

    /// <summary>
    /// This is the method where the actual text writing into file is done.
    /// </summary>
    /// <param name="sFolderPath"></param>
    /// <param name="FileName"></param>
    /// <param name="sErrMsg"></param>
    /// <remarks></remarks>
    private static void ErrorLog(string sFolderPath, string FileName, string sErrMsg)
    {
        StreamWriter sw = null;
        string sPathName = Path.Combine(sFolderPath, FileName);
        try
        {
            MoveContent(sFolderPath, FileName);
            if (!File.Exists(sPathName))
            {
                // Create a file to write to.
                sw = File.CreateText(sPathName);
                object style = "<STYLE TYPE=\"text/css\">";
                style = (style + "BODY{font-family:Calibri;");
                style = (style + "font-size: 14px;font-weight: Bold;}");
                style = (style + "</STYLE>");
                sw.WriteLine(style);
            }
            else
            {
                // This text is always added, making the file longer over time
                // if it is not deleted.
                //using (StreamWriter sw = File.AppendText(sPathName))
                sw = File.AppendText(sPathName);
            }
            sw.WriteLine(sErrMsg + "<br>");
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
        }

    }

    /// <summary>
    /// If log file size becomes large then an archive file is created and log file starts from zero size.
    /// </summary>
    /// <param name="sFolderPath"></param>
    /// <param name="FileName"></param>
    /// <remarks></remarks>
    private static void MoveContent(string sFolderPath, string FileName)
    {
        FileStream fs = null;
        string sPathName = Path.Combine(sFolderPath, FileName);
        try
        {
            string prvslogfilename = null;
            prvslogfilename = Path.Combine(Folder, "JHDataImpoterLog.html");
            fs = System.IO.File.Open(sPathName, FileMode.Open, FileAccess.Read);
            //1MB =>1024 X KB
            //5MB =>1024 X 5
            //5242880 5MB
            // 4MB 409600
            if (fs.Length >= 5242880)
            {
                //limit to 512MB
                fs.Close();
                fs.Dispose();
                if (File.Exists(prvslogfilename))
                {
                    //remove PreviousLog  
                    System.IO.File.Delete(prvslogfilename);
                }
                //System.IO.File.Delete(sPathName);    
                System.IO.File.Move(sPathName, prvslogfilename);
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (fs != null)
            {
                fs.Close();
            }
        }
    }
}
